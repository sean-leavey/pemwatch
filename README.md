# PEM watchdog tools
Pemwatch is a system to monitor physical environment channels in a lab. It supports data retrieval
over [NDS](https://www.lsc-group.phys.uwm.edu/daswg/projects/nds-client.html) but can work with any
other technique, and can optionally post warnings to an external WordPress logbook.

Pemwatch is modular and highly configurable using its configuration file. Arbitrary numbers of
watchdogs can be set up, and their configurations can be shared or inherited using the powerful
syntax provided by [YAML](https://yaml.org/). Watchdogs can ingest data to check using NDS, but it
can also use any other mechanism as long as appropriate application logic is provided.
Available checks are whether data is changing and whether it is above or below a threshold, but,
again, arbitrary checks can be defined by implementing the corresponding logic.
Alerts can be configured to post plots and text to an external WordPress logbook using the WordPress
API, with authentication provided by either an application password (provided by e.g.
[ALP](https://alp.attackllama.com/)) or a simple username and password. Back-off timeouts can be
configured to prevent long failures from spamming the logbook with repeated failure messages.

## Requirements
Requires Python 3.5+, `gwpy`, `gpstime` and other libraries. See `setup.py` for the full list.

## Usage
Copy `config.yaml.dist` into a new file, e.g. `config.yaml`. This file can go anywhere you want.
Customise the configuration. It uses [YAML](https://yaml.org/) syntax, which is quite powerful. You
can, for example, copy configurations from other blocks in the same file and override particular
sets of parameters. See [this page](https://learnxinyminutes.com/docs/yaml/) for details.

### Scheduled execution
Pemwatch is best configured to run on a regular basis automatically. On Linux this is trivial to
achieve with e.g. systemd or crontab. The examples below show how to configure pemwatch to run every
15 minutes. Ensure to change `framebuilder:8088` to whatever your framebuilder host uses, and
`USERNAME` to the normal CDS user.

#### Systemd service and timer
A user service can be created with `systemctl --user edit --full --force pemwatch.service` with the
following contents:

```
[Unit]
Description=Pemwatch
Wants=network-online.target
After=network-online.target

[Service]
Type=oneshot
ProtectSystem=strict
Restart=no
Environment=LANG=C.UTF-8
Environment=NDSSERVER=framebuilder:8088
ExecStart=/usr/bin/python3 -m pemwatch -c pemwatch.yaml -l pemwatch.lock -v
```

And a corresponding timer can be created with `systemctl --user edit --full --force pemwatch.timer`
with the following contents:

```
[Unit]
Description=Run Pemwatch

[Timer]
OnCalendar=*:0/15
Persistent=true

[Install]
WantedBy=timers.target
```

Start the timer with `systemctl --user start pemwatch.timer` and enable on boot with `systemctl
--user enable pemwatch.timer`. Check the timer is scheduled with `systemctl --user list-timers`.

Normally systemd is configured to not allow non-root user services to run when logged out. This
obviously prevents the timer running as intended. This behaviour can be switched off for the
relevant user with `sudo loginctl enable-linger USERNAME`.

Logs can be viewed with `journalctl --user-unit pemwatch`. Note that the user has to be a member of
the `systemd-journal` group to see these (`sudo usermod -a -G systemd-journal USERNAME`).

#### Crontab
One caveat to watch out for when using a cronjob is to ensure that only one instance of Pemwatch is
running at a given time. This can be achieved with the utility `flock`. Here is an example of a
crontab entry that runs every 15 minutes:

```
*/15 * * * * NDSSERVER=framebuilder:8088 flock --nonblock -n .pemwatch.pid -c "python3 -m pemwatch -c pemwatch.yaml -l pemwatch.lock"
```

### WordPress integration
Pemwatch can be configured to post plots to a WordPress blog. It requires application passwords
to be available on the blog, provided by e.g. [Application Passwords](https://wordpress.org/plugins/application-passwords/)
or [Academic Labbook Plugin](https://alp.attackllama.com/). Simple username and password
authentication is also possible, but this is not recommended as it gives full logbook access to
anyone that can read the configuration file or intercept the network traffic.

To configure WordPress integration, see the `wp_plot_post` consumer type in the example
configuration.

## Credits
Sean Leavey  
<sean.leavey@ligo.org>
