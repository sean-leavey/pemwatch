"""Watchdog result consumers"""

import abc
import logging
import hashlib
from dateutil.tz import tzlocal
import gpstime
from .config import Config

LOGGER = logging.getLogger(__name__)


def get_consumer(name, watchdog, lockfile, repeat_delay=None, attributes=None,
                 **consumer_attributes):
    """Get the relevant consumer given the specified arguments."""
    cname = name.lower()
    if attributes is None:
        attributes = {}
    if cname == "wp_post":
        from .contrib.wordpress import WordPressPostConsumer
        # Merge attributes, overwriting general ones where appropriate.
        kwargs = {**consumer_attributes, **attributes}
        return WordPressPostConsumer(
            watchdog=watchdog, lockfile=lockfile, repeat_delay=repeat_delay, **kwargs
        )
    elif cname == "wp_plot_post":
        from .contrib.wordpress import WordPressPlotPostConsumer
        # Merge attributes, overwriting general ones where appropriate.
        kwargs = {**consumer_attributes, **attributes}
        return WordPressPlotPostConsumer(
            watchdog=watchdog, lockfile=lockfile, repeat_delay=repeat_delay, **kwargs
        )
    raise ValueError("unrecognised consumer: '%s'" % name)


class BaseConsumer(metaclass=abc.ABCMeta):
    def __init__(self, watchdog, lockfile, repeat_delay=None):
        self.watchdog = watchdog
        self.lockfile = lockfile
        self.repeat_delay = repeat_delay
        self._now = None

    @property
    def hashpieces(self):
        return (*self.watchdog.hashpieces, self.__class__.__name__)

    @property
    def hash(self):
        """Consumer hash function.

        This function is stable across Python instances, allowing it to be serialised,
        e.g. into a lockfile.
        """
        hashfunc = hashlib.sha256()
        piecestr = '-'.join([str(piece) for piece in self.hashpieces])
        hashfunc.update(piecestr.encode("ascii"))
        return hashfunc.hexdigest()

    def run(self, failed, dry_run=False, **kwargs):
        LOGGER.info("Running consumer '%s'" % self)
        # Set current time.
        self._now = gpstime.gpstime.now().replace(tzinfo=tzlocal())

        if not failed:
            # Watchdog passed.
            self._set_repeat(remove=True)
            return
        if self._check_repeat():
            LOGGER.info("Consumer '%s' will not execute its actions because not enough time has "
                        "passed since the last failure.", self)
            return
        self._do_failure_run(dry_run=dry_run, **kwargs)
        self._set_repeat()

    def _set_repeat(self, remove=False):
        """Update lockfile entry."""
        if remove:
            LOGGER.debug("Removing last repeat time from %s", self.lockfile)
        else:
            LOGGER.debug("Updating last repeat time in %s", self.lockfile)
        try:
            with open(self.lockfile, "r") as fobj:
                lines = fobj.readlines()
        except FileNotFoundError:
            LOGGER.debug("Lockfile not found")
            lines = []
        selfhash = self.hash
        newlines = []
        for line in lines:
            line = line.strip()
            if not line:
                # Empty line.
                continue
            serial, _, _ = self._parse_lock_line(line)
            if serial != selfhash:
                newlines.append(line)
        if not remove:
            # Add new line at end.
            newlines.append(self._get_lock_line())
        with open(self.lockfile, "w") as fobj:
            fobj.write("\n".join(newlines))

    def repeat_epoch(self, last_fail):
        """Gets the datetime object representing the last failure date plus the configured delay."""
        if self.repeat_delay is None:
            return last_fail
        # Hack: in order to allow human time differences, here we parse the repeat delay as a full
        # date and subtract current date.
        repeat_delay = gpstime.parse(self.repeat_delay) - self._now
        return last_fail + repeat_delay

    def _check_repeat(self):
        LOGGER.debug("Checking last repeat time in %s", self.lockfile)
        try:
            with open(self.lockfile, "r") as fobj:
                lines = fobj.readlines()
        except FileNotFoundError:
            LOGGER.debug("Lockfile not found")
            lines = []
        selfhash = self.hash
        for line in lines:
            line = line.strip()
            if not line:
                # Empty line.
                continue
            serial, _, last_fail = self._parse_lock_line(line)
            if serial != selfhash:
                # This line is for a different consumer.
                continue
            LOGGER.debug("Last failure found at %s", last_fail)
            repeat_epoch = self.repeat_epoch(last_fail)
            newer = repeat_epoch > self._now
            if newer:
                LOGGER.debug("Last failure in lockfile plus repeat delay (%s) is in the "
                             "future (now = %s)", repeat_epoch, self._now)
            else:
                LOGGER.debug("Last failure in lockfile plus repeat delay (%s) is in the "
                             "past (now = %s)", repeat_epoch, self._now)
            return newer
        LOGGER.debug("No line found for hash %s (%d lines total)", selfhash, len(lines))
        return False

    def _get_lock_line(self):
        return "%s %s %s" % (self.hash, self.__class__.__name__, gpstime.gpsnow())

    def _parse_lock_line(self, line):
        serial, classname, time = line.split()
        time = gpstime.parse(time)
        return serial, classname, time

    @abc.abstractmethod
    def _do_failure_run(self, dry_run=False):
        raise NotImplementedError

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.watchdog.description)
