"""PEM watchdog tools command line interface"""

import sys
import os
import logging
from logging.handlers import TimedRotatingFileHandler
import click

from . import __version__, PROGRAM, DESCRIPTION, add_log_handler
from .config import Config
from .watchdog import get_watchdog


# Shared arguments:
# https://github.com/pallets/click/issues/108
class State:
    """CLI state"""
    MIN_VERBOSITY = logging.WARNING
    MAX_VERBOSITY = logging.DEBUG

    def __init__(self):
        self.handler = logging.StreamHandler()
        add_log_handler(self.handler)
        self._verbosity = self.MIN_VERBOSITY

    @property
    def verbosity(self):
        """Verbosity on stdout"""
        return self._verbosity

    @verbosity.setter
    def verbosity(self, verbosity):
        self._verbosity = self.MIN_VERBOSITY - 10 * int(verbosity)
        if self._verbosity < self.MAX_VERBOSITY:
            self._verbosity = self.MAX_VERBOSITY
        self.handler.setLevel(self._verbosity)

    @property
    def verbose(self):
        """Verbose output enabled

        Returns True if the verbosity is enough for INFO or DEBUG messages to be displayed.
        """
        return self.verbosity <= logging.INFO


def set_verbosity(ctx, _, value):
    """Set stdout verbosity"""
    state = ctx.ensure_object(State)
    state.verbosity = value

@click.command(help=DESCRIPTION)
@click.version_option(version=__version__, prog_name=PROGRAM)
@click.option("-v", "--verbose", count=True, default=0, callback=set_verbosity, expose_value=False,
              help="Enable verbose output. Supply extra flag for greater verbosity, i.e. \"-vv\".")
@click.option("-c", "--config-file", type=click.Path(exists=True, dir_okay=False),
              envvar='PEMWATCH_CONF', help="Path to pemwatch configuration file. If not specified, "
              "the environment variable PEMWATCH_CONF is searched.")
@click.option("-l", "--lock-file", type=click.Path(dir_okay=False),
              envvar='PEMWATCH_LOCK', help="Path to pemwatch lock file. If not specified, the "
              "environment variable PEMWATCH_LOCK is searched.")
@click.option("--nds-host", type=str, envvar="NDSSERVER", help="NDS server URL and port. If not "
              "specified, the environment variable NDSSERVER is searched.")
@click.option("--dry-run", is_flag=True, default=False, help="Don't actually run the watchdog "
              "actions.")
@click.option("--debug", is_flag=True, default=False, help="Show full debug traces.")
@click.pass_context
def pemwatch(ctx, config_file, lock_file, nds_host, dry_run, debug):
    """Base pemwatch command group"""
    state = ctx.ensure_object(State)
    config = Config()
    config.load(config_file)
    _configure_log_handlers(config["loggers"])
    # Construct watchdog parameters.
    try:
        host, port = nds_host.split(":")
    except:
        click.echo("The 'nds-host' parameter is invalid, was not specified, or cannot be found as "
                   "an environment variable ($NDSSERVER). Its format should be 'HOST:PORT'. Please "
                   "specify this parameter to continue.", err=True, color="red")
        if debug:
            raise
        sys.exit(1)
    if dry_run:
        # Replace lockfile with empty black hole.
        click.echo("--dry-run set; replacing specified lock file with null file")
        lock_file = os.devnull
    for watchdog_config in config["watchdogs"]:
        kwargs = {"host": host, "port": int(port), "lock_file": lock_file, **watchdog_config,
                  "consumer_config": config["consumers"]}
        watchdog = get_watchdog(**kwargs)
        try:
            watchdog.run(dry_run=dry_run)
        except Exception as exception:
            click.echo("Watchdog '%s' failed to run: %s" % (watchdog, exception), err=True,
                       color="red")
            if debug:
                raise
            sys.exit(1)

def _configure_log_handlers(loggers):
    """Configure additional log handlers.

    This configures log handlers in addition to the basic stream handler defined in __init__.py.
    """
    for logger in loggers:
        # Copy dict to avoid modifying it between loops.
        data = dict(logger)
        level = logging.getLevelName(data["level"].upper())
        del data["level"]
        handler_kwargs = {}
        if "format" in data:
            handler_kwargs["format_str"] = data["format"]
            del data["format"]
        # Replace camel case option.
        data["backupCount"] = data.get("backup_count", 0)
        data.pop("backup_count", None)
        handler = TimedRotatingFileHandler(**data)
        handler.setLevel(level)
        add_log_handler(handler, **handler_kwargs)


if __name__ == "__main__":
    pemwatch()
