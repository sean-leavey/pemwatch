"""Config reader"""

import os
from yaml import safe_load


class ConfigError(ValueError):
    def __init__(self, message, *args, **kwargs):
        super().__init__("Configuration error: %s" % message, *args, **kwargs)


class Config(dict):
    """YAML config parser

    Implements the Borg pattern: many instances, one state.
    """
    __shared_state = {}

    def __init__(self):
        self.__dict__ = self.__shared_state

    def __getitem__(self, key):
        if not hasattr(self, "config") or self.config is None:
            raise Exception("config not yet loaded")
        return self.config[key]

    def load(self, path):
        if not os.path.isfile(path):
            raise Exception("%s is not a file" % path)
        with open(path, "r", encoding="utf-8") as configfile:
            self.config = safe_load(configfile)

    def __repr__(self):
        return str(self.config)
