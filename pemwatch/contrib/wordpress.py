"""WordPress tools"""

import os
import abc
import logging
from html import escape
from tempfile import TemporaryDirectory
from wordpress import API
from ..consumer import BaseConsumer

LOGGER = logging.getLogger(__name__)


class WordPressAPI:
    def __init__(self, config):
        self.config = config
        self._api = None

    def __str__(self):
        return "WordPress(%s)" % self.api.url

    def __repr__(self):
        return str(self)

    @property
    def request_settings(self):
        return self.config.get("request_settings", {})

    @property
    def api(self):
        if self._api is None:
            self._api = API(
                url=self.config["host"],
                consumer_key="",
                consumer_secret="",
                api="wp-json",
                version="wp/v2",
                wp_user=self.config["username"],
                wp_pass=self.config["password"],
                basic_auth=True,
                user_auth=True,
            )
        return self._api

    def create_post(self, title, body, categories, tags, post_format, post_status):
        endpoint = "/posts"
        data = {
            "title": escape(title),
            "content": body,
            "categories": categories,
            "tags": tags,
            "format": post_format,
            "status": post_status,
        }
        response = self.api.post(endpoint, data, **self.request_settings)
        LOGGER.info("Create post response status: %i" % response.status_code)
        return response

    def create_attachment(self, path, **kwargs):
        if not os.path.exists(path):
            raise ValueError("file '%s' does not exist" % path)
        with open(path, "rb") as fobj:
            data = fobj.read()
        filename = os.path.basename(path)
        _, extension = os.path.splitext(filename)
        headers = {
            "cache-control": "no-cache",
            "content-disposition": "attachment; filename=%s" % filename,
            "content-type": "image/%s" % extension
        }
        endpoint = "/media"
        response = self.api.post(endpoint, data, headers=headers, **self.request_settings)
        LOGGER.info("Create attachment response status: %i" % response.status_code)
        return response


class WordPressPostConsumer(BaseConsumer):
    def __init__(self, post_to, template, endpoints, **kwargs):
        super().__init__(**kwargs)
        self.template = template
        if post_to not in endpoints:
            raise ValueError("Endpoints must contain post_to key")
        self.endpoint = endpoints[post_to]

    def _do_failure_run(self, dry_run=False, **body_format_kwargs):
        if dry_run:
            # Let's not generate talk to WordPress.
            LOGGER.info("Skipping post upload because of dry run")
            return
        # Escape format kwargs.
        for key, value in body_format_kwargs.items():
            try:
                body_format_kwargs[key] = escape(value)
            except:
                # Value cannot be escaped.
                pass
        self._upload(**body_format_kwargs)

    def _upload(self, **kwargs):
        post_response = self._create_post(**kwargs)
        if not post_response.status_code == 201:
            LOGGER.error("Could not upload post - skipping %s", self)
            return

    def _create_post(self, **body_format_kwargs):
        body = self.template["body"].format(**body_format_kwargs)
        wp = WordPressAPI(self.endpoint)
        LOGGER.info("Sending post '%s' to %s" % (self.template["title"], wp))
        response = wp.create_post(
            title=self.template["title"],
            body=body,
            categories=self.template["categories"],
            tags=self.template["tags"],
            post_format=self.template["post_format"],
            post_status=self.template["post_status"]
        )
        if response.status_code != 201:
            LOGGER.error("Could not create post '%s' at %s" % (self.template["title"], self.endpoint))
        return response

    def _create_file(self, path):
        wp = WordPressAPI(self.endpoint)
        LOGGER.info("Sending media %s to %s" % (path, wp))
        response = wp.create_attachment(path)
        if response.status_code != 201:
            LOGGER.error("Could not create media %s at %s" % (path, self.endpoint))
        return response


class WordPressPlotPostConsumer(WordPressPostConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not hasattr(self.watchdog, 'plot'):
            raise ValueError("The WordPress plot consumer's watchdog must support plots")

    def _upload(self, **kwargs):
        # Generate plots.
        figs = self.watchdog.plot()
        # Save.
        with TemporaryDirectory(prefix="pemwatch") as tmpdir:
            paths = []
            for index, fig in enumerate(figs, start=1):
                filename = "%s %i.png" % (self.watchdog.description, index)
                path = os.path.join(tmpdir, filename)
                paths.append(path)
                fig.savefig(path)
                LOGGER.info("Saved plot to %s" % path)
            image_responses = []
            for path in paths:
                image_responses.append(self._create_file(path))
        if not all([img_response.status_code == 201 for img_response in image_responses]):
            LOGGER.error("Could not upload all attachments - skipping %s", self)
            return
        images_json = [image_response.json() for image_response in image_responses]
        plots = self._create_plot_html(images_json)

        return super()._upload(plots=plots, **kwargs)

    def _create_plot_html(self, images_json):
        html = ""
        for image in images_json:
            html += self.template["image_body"].format(
                post_id=int(image["id"]),
                post_fullsize_url=escape(image["media_details"]["sizes"]["full"]["source_url"]),
                post_smallsize_url=escape(image["media_details"]["sizes"]["medium_large"]["source_url"]),
            )
        return html
