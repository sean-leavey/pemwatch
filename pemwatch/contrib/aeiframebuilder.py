"""AEI framebuilder watchdog

Note: this uses AEI formatted framebuilder restart logfile.
"""

import abc
import logging
from gpstime import gpstime
from ..watchdog import Watchdog
from ..check import Check, CheckTree
from ..util import readlines_reversed

LOGGER = logging.getLogger(__name__)


class AEIFramebuilderCheck(Check, metaclass=abc.ABCMeta):
    """Base AEI framebuilder check

    This defines the interface to perform a framebuilder check.
    """
    NAME = "aei_framebuilder_base_check"

    def __init__(self, value, **kwargs):
        super().__init__(**kwargs)
        self.value = value

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.value)


class AEIFramebuilderHasRestarted(AEIFramebuilderCheck):
    NAME = "has_restarted"

    def __init__(self, value=True, **kwargs):
        super().__init__(value=value, **kwargs)

    def evaluate(self, restart_times, **kwargs):
        return (len(restart_times) > 0) == self.value

    @property
    def description(self):
        return "%s()" % self.NAME


class AEIFramebuilderCheckTree(CheckTree, AEIFramebuilderCheck):
    """Expression tree for AEI framebuilder check conditions."""
    NAME = "aei_framebuilder_check_tree"
    CHECK_TYPES = [AEIFramebuilderHasRestarted]


class AEIFramebuilderWatchdog(Watchdog):
    """AEI framebuilder restart watchdog."""
    # Type to handle the check tree.
    CHECKTREE = AEIFramebuilderCheckTree

    def __init__(self, restart_log_file, **kwargs):
        self.restart_log_file = restart_log_file
        self._restart_times = None
        super().__init__(**kwargs)

    @property
    def _consumer_kwargs(self):
        return {
            **super()._consumer_kwargs,
            "restarts": ", ".join([str(time) for time in self._get_restart_times()]),
            "restart_log_file": self.restart_log_file,
        }

    def _get_restart_times(self, force=False):
        if self._restart_times is None or force:
            start = self.check_period["start"]
            end = self.check_period["end"]
            restarts = []
            prefix = "daqd_start"
            # Read lines in reverse order and terminate once we go beyond the start time.
            for line in readlines_reversed(self.restart_log_file):
                if not line.startswith(prefix):
                    continue
                # Parse the rest of the line as a date.
                restart = gpstime.parse(line[len(prefix)+1:])
                if restart >= end:
                    # This time is later than the end time.
                    continue
                if restart < start:
                    # We've gone beyond the limits.
                    break
                # This restart is within the bounds.
                LOGGER.debug("framebuilder restart found at %s" % restart)
                restarts.append(restart)
            self._restart_times = restarts
            LOGGER.debug("Found %i framebuilder restart(s)", len(self._restart_times))
        return self._restart_times

    def _evalulate_check(self, check):
        restart_times = self._get_restart_times()
        return check.evaluate(restart_times)
