"""NDS channel watchdogs"""

import abc
import logging
from functools import reduce
from collections import Iterable
import numpy as np
from gwpy.segments import DataQualityDict
from gwpy.timeseries import TimeSeries
from gwpy.io import nds2
from gwpy.plot import Plot
from ..watchdog import Watchdog
from ..check import Check, CheckTree

LOGGER = logging.getLogger(__name__)


class NDSData(metaclass=abc.ABCMeta):
    def __init__(self, host, port, **kwargs):
        super().__init__(**kwargs)
        self.host = host
        self.port = port

    def fetch_data(self, channel, start, end, pad=np.nan):
        return TimeSeries.fetch(start=start, end=end, channel=channel, host=self.host,
                                port=self.port, pad=pad)


class NDSCheck(Check, metaclass=abc.ABCMeta):
    """Base NDS check

    This defines the interface to perform a check on some NDS data and to return a data quality flag
    representing the check for use in plots, etc.
    """
    NAME = "nds_base_check"

    def __init__(self, value, fail_threshold=0, is_good=False, **kwargs):
        super().__init__(**kwargs)
        self.value = value
        self.fail_threshold = fail_threshold
        self.is_good = is_good

    def active_fraction(self, data, **kwargs):
        flag = self.dqflag(data, **kwargs)
        return abs(flag.active) / abs(flag.known)

    def evaluate(self, data, **kwargs):
        fraction = self.active_fraction(data, **kwargs)
        result = fraction >= self.fail_threshold
        LOGGER.debug("Check %s fraction is %.5f, threshold is %.5f, evaluated to %r"
                     % (self, fraction, self.fail_threshold, result))
        return result

    def dqflag(self, data, **kwargs):
        flag = self._state_vector(data).to_dqflag(**kwargs)
        flag.isgood = self.is_good
        return flag

    def base_dqflags(self, data, **kwargs):
        return self.dqflag(data, **kwargs)

    def _state_vector(self, data):
        raise NotImplementedError

    def __str__(self):
        return "%s(value=%s th=%s)" % (self.__class__.__name__, self.value, self.fail_threshold)


class NDSIsChangingCheck(NDSCheck):
    """Check for whether a channel data set contains changing values

    Be careful: using this check with data with a stop time set to the current time might result in
    a data set containing padded data because the corresponding frames might not have been made
    available to NDS yet. In such a case, the gradient calculation may be affected and could lead to
    incorrect results. To avoid this, query data a few minutes in the past to give the frame builder
    time to save the data.
    """

    NAME = "is_changing"

    def __init__(self, **kwargs):
        super().__init__(is_good=False, value=None, **kwargs)

    def _state_vector(self, data):
        # Check if gradient is zero.
        return np.gradient(data) == 0 * data.unit

    @property
    def description(self):
        start = "" if self.value else "!"
        return "%s%s(channel, threshold=%s)" % (start, self.NAME, self.fail_threshold)


class NDSAboveThresholdCheck(NDSCheck):
    NAME = "above_threshold"

    def _state_vector(self, data):
        return data > self.value * data.unit

    @property
    def description(self):
        return "%s(channel > %s, threshold=%s)" % (self.NAME, self.value, self.fail_threshold)


class NDSBelowThresholdCheck(NDSCheck):
    NAME = "below_threshold"

    def _state_vector(self, data):
        return data < self.value * data.unit

    @property
    def description(self):
        return "%s(channel < %s, threshold=%s)" % (self.NAME, self.value, self.fail_threshold)


class NDSCheckTree(CheckTree, NDSCheck):
    """Expression tree for NDS check conditions."""
    NAME = "nds_check_tree"
    CHECK_TYPES = [NDSIsChangingCheck, NDSAboveThresholdCheck, NDSBelowThresholdCheck]

    def dqflag(self, data, **kwargs):
        branch_flags = [branch.dqflag(data) for branch in self.branches]
        return reduce(self._binary_operator, branch_flags)

    def base_dqflags(self, data, **kwargs):
        for branch in self.branches:
            flag = branch.base_dqflags(data, **kwargs)
            if isinstance(flag, Iterable):
                yield from flag
            else:
                yield flag


class NDSWatchdog(NDSData, Watchdog, metaclass=abc.ABCMeta):
    """Base NDS watchdog."""
    # Type to handle the check tree.
    CHECKTREE = NDSCheckTree

    def __init__(self, channel, **kwargs):
        self.channel = channel
        super().__init__(**kwargs)

    @property
    def _consumer_kwargs(self):
        return {
            **super()._consumer_kwargs,
            "channel": self.channel,
        }

    def _evalulate_check(self, check):
        data = self.fetch_data(self.channel, **self.check_period)
        return check.evaluate(data)

    def active_fraction(self, data, **kwargs):
        return self.check.active_fraction(data, **kwargs)

    def dq_flag(self, data, **kwargs):
        return self.check.dqflag(data, **kwargs)

    def dq_flag_dict(self, data, **kwargs):
        flags = list(self.check.base_dqflags(data, **kwargs))
        names = range(len(flags))
        return DataQualityDict(dict(zip(names, flags)))


class NDSTimeSeriesWatchdog(NDSWatchdog):
    def __init__(self, plots=None, plot_period=None, add_segments_bar=True, **kwargs):
        super().__init__(**kwargs)
        self._plot_period = None
        self.plots = plots
        self.add_segments_bar = add_segments_bar

        if self.plots is not None:
            self.plot_period = plot_period

    @property
    def plot_period(self):
        return self._plot_period

    @plot_period.setter
    def plot_period(self, bounds):
        # Parse strings into dates so that e.g. "now" doesn't change during execution.
        self._plot_period = self._parse_bounds(bounds)

    @property
    def plot_options(self):
        return {}

    def plot(self):
        if self.plot is None:
            LOGGER.info("No plots specified")
            return

        LOGGER.info("Generating plot(s)")
        return [self._make_plot(plot) for plot in self.plots]

    def _make_plot(self, plot):
        if "plot" in plot:
            return self._make_single_plot(plot["plot"])
        raise ValueError("unrecognised plot type")

    def _make_single_plot(self, config):
        channels = config["channels"]
        LOGGER.info("Creating single plot for %i channel(s)" % len(channels))
        extradata = [self.fetch_data(channel=channel, **self.plot_period) for channel in channels
                     if channel != self.channel]
        if self.channel in channels:
            primarydata = self.fetch_data(channel=self.channel, **self.plot_period)
        else:
            primarydata = None
        plotdata = [primarydata] if primarydata is not None else []
        plotdata.extend(extradata)
        # Make plot.
        plot = Plot(*plotdata, **config["options"])
        ax = plot.gca()
        if "trendlines" in config:
            # Add trendlines.
            for line in config["trendlines"]:
                if "axis" not in line:
                    raise ValueError("Trendline config must contain 'axis' key.")
                if line["axis"].lower() == "x":
                    method = ax.axvline
                elif line["axis"].lower() == "y":
                    method = ax.axhline
                else:
                    raise ValueError("Invalid trendline axis: '%s'" % line["axis"])
                method(line["position"], **line.get("options", {}))
        if self.add_segments_bar and primarydata is not None:
            root_flag = self.dq_flag(primarydata)
            # Set the label to the fail percentage.
            fraction = self.active_fraction(primarydata)
            root_flag.label = "%.1f%%" % (fraction * 100)
            LOGGER.info("Adding segment bar")
            plot.add_segments_bar(root_flag)
        # Add legend.
        ax.legend([str(data.channel) for data in plotdata])
        return plot
