"""Base watchdog"""

import os
import abc
import logging
import traceback
import numpy as np
from gpstime import gpstime
from .config import ConfigError
from .check import CheckTree
from .consumer import get_consumer

LOGGER = logging.getLogger(__name__)


def get_watchdog(**kwargs):
    """Get the relevant watchdog given the specified arguments."""
    data_type = kwargs["data_type"].lower()
    kwargs.pop("data_type", None)
    if data_type == "nds_timeseries":
        from .contrib.nds import NDSTimeSeriesWatchdog
        return NDSTimeSeriesWatchdog(**kwargs)
    elif data_type == "aei_framebuilder":
        from .contrib.aeiframebuilder import AEIFramebuilderWatchdog
        return AEIFramebuilderWatchdog(**kwargs)
    raise ValueError("unrecognised data type: '%s'" % kwargs["data_type"])


class Watchdog(metaclass=abc.ABCMeta):
    """Base watchdog."""
    # Type to handle the check tree.
    CHECKTREE = CheckTree

    def __init__(self, description, notice, check, check_period, lock_file, consumers,
                 consumer_config, precheck=None, **kwargs):
        self._check_period = None
        self.raw_check_period = None

        if precheck is not None:
            precheck = self.CHECKTREE.from_list(precheck, "and")

        self.description = description
        self.notice = notice
        self.precheck = precheck
        self.check = self.CHECKTREE.from_list(check, "and")
        self.check_period = check_period
        self.consumers = []

        for consumer in consumers:
            consumer_type = consumer["consumer_type"]
            if consumer_type not in consumer_config:
                raise ConfigError("consumer '%s' is not configured" % consumer_type)
            # Overwrite global consumer config with local one.
            config = {**consumer_config[consumer_type], **consumer}
            config.pop("consumer_type", None)
            self.consumers.append(get_consumer(consumer_type, self, lock_file, **config))

        LOGGER.info("Initialised watchdog '%s' checking between %s and %s"
                    % (self.description, self.check_period["start"], self.check_period["end"]))

    @property
    def _consumer_kwargs(self):
        return {
            "description": self.description,
            "notice": self.notice,
            "precheck_description": getattr(self.precheck, "description", "(none)"),
            "check_description": getattr(self.check, "description", "(none)"),
            "start_date": self.check_period["start"],
            "end_date": self.check_period["end"],
            "start_raw": self.raw_check_period["start"],
            "end_raw": self.raw_check_period["end"],
        }

    @property
    def hashpieces(self):
        return self.description, self.__class__.__name__

    @property
    def check_period(self):
        return self._check_period

    @check_period.setter
    def check_period(self, check_period):
        # Parse strings into dates so that e.g. "now" doesn't change during execution.
        self._check_period = self._parse_bounds(check_period)
        self.raw_check_period = check_period

    def run(self, dry_run=False):
        if dry_run:
            LOGGER.info("Performing '%s' DRY RUN watchdog checks", self)
        else:
            LOGGER.info("Performing '%s' watchdog checks", self)
        LOGGER.info("Checking between %s and %s", self.check_period["start"],
                    self.check_period["end"])
        if self.precheck is not None:
            LOGGER.info("Performing precheck")
            passed = self._do_check(self.precheck)
            if not passed:
                LOGGER.info("Precheck failed, stopping watchdog")
                return
            else:
                LOGGER.info("Precheck passed")
        else:
            LOGGER.debug("No precheck defined")
        LOGGER.info("Performing main check")
        failed = self._do_check(self.check)
        if failed:
            LOGGER.info("Watchdog '%s' did not pass.", self)
        else:
            LOGGER.info("Watchdog '%s' passed.", self)
        for consumer in self.consumers:
            try:
                consumer.run(failed, dry_run=dry_run, **self._consumer_kwargs)
            except Exception as exception:
                LOGGER.error("Consumer '%s' failed to run. No changes have been made.", consumer)
                LOGGER.debug("Traceback: %s", "".join(traceback.format_tb(exception.__traceback__)))
                raise exception
            else:
                LOGGER.info("Consumer '%s' successfully ran.", consumer)

    def _do_check(self, check):
        result = self._evalulate_check(check)
        LOGGER.info("Check '%s' in '%s' evaluated to %s" % (check, self, result))
        return result

    @abc.abstractmethod
    def _evalulate_check(self, check):
        raise NotImplementedError

    def _parse_bounds(self, bounds):
        return {"start": self._parse_time(bounds["start"]),
                "end": self._parse_time(bounds["end"])}

    def _parse_time(self, time):
        try:
            return gpstime.parse(time)
        except:
            pass
        return time

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.description)
