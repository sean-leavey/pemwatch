"""Checks and check trees"""

import abc
import logging
from functools import reduce
import textwrap
import operator

LOGGER = logging.getLogger(__name__)


class Check(metaclass=abc.ABCMeta):
    """Base check

    This defines the interface to perform an arbitrary check of some condition.
    """
    NAME = "base_check"

    @abc.abstractmethod
    def evaluate(self, *args, **kwargs):
        raise NotImplementedError

    @abc.abstractmethod
    def __str__(self):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def description(self):
        raise NotImplementedError


class CheckTree(Check):
    """Expression tree for check conditions."""
    NAME = "check_tree"

    OPERATORS = {
        "and": operator.and_,
        "or": operator.or_,
    }

    CHECK_TYPES = []

    def __init__(self, expression):
        exp = expression.lower()
        if exp not in self.OPERATORS:
            raise ValueError("expression '%s' not supported" % expression)
        self.expression = exp
        self.branches = []

    def add_branch(self, branch):
        if branch in self.branches:
            raise ValueError("branch '%s' already exists" % branch)
        self.branches.append(branch)

    @classmethod
    def from_list(cls, tree, expression):
        obj = cls(expression)
        for branch in tree:
            keys = list(branch.keys())
            if not keys:
                raise ValueError("check list entries must have items")
            subexp = keys[0].lower()
            if subexp in cls.OPERATORS:
                # This is an expression tree.
                if len(keys) != 1:
                    raise ValueError("expression trees must have exactly one key")
                # Grab the corresponding list.
                values = list(branch.values())[0]
                obj.add_branch(cls.from_list(values, subexp))
            else:
                # Assume direct check.
                obj.add_branch(cls.get_check(branch))
        return obj

    @classmethod
    def get_check(cls, check):
        check_type = check["check_type"].lower()
        params = dict(check)
        # Remove check type if it exists.
        params.pop("check_type", None)
        check_types = {obj.NAME: obj for obj in cls.CHECK_TYPES}
        if check_type in check_types:
            obj = check_types[check_type]
            return obj(**params)
        raise ValueError("unrecognised check type '%s'" % check["check_type"])

    @property
    def _binary_operator(self):
        return self.OPERATORS[self.expression]

    def evaluate(self, data, **kwargs):
        operation = self._binary_operator
        branch_evaluations = [branch.evaluate(data, **kwargs) for branch in self.branches]
        return reduce(operation, branch_evaluations)

    def __str__(self):
        branches = ", ".join([str(branch) for branch in self.branches])
        return "%s(%s(%s))" % (self.__class__.__name__, self.expression, branches)

    @property
    def description(self):
        branches = ",\n".join([textwrap.indent(branch.description, "  ")
                               for branch in self.branches])
        return """%s(
%s
)""" % (self.expression.upper(), branches)
