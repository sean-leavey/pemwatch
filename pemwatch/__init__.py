"""PEM watchdog tools"""

import logging

__version__ = "0.3.0"
PROGRAM = "Pemwatch"
AUTHOR = "Sean Leavey <sean.leavey@ligo.org>"
DESCRIPTION = """PEM Watchdog tools"""
DEFAULT_LOG_FORMAT = "%(name)-25s - %(levelname)-8s - %(message)s"

# Create base logger.
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
logging.captureWarnings(True)

# Suppress warnings when the user code does not include a handler.
LOGGER.addHandler(logging.NullHandler())

def add_log_handler(handler, format_str=None):
    if format_str is None:
        format_str = DEFAULT_LOG_FORMAT
    handler.setFormatter(logging.Formatter(format_str))
    LOGGER.addHandler(handler)

# Set up Matplotlib for headless operation.
import matplotlib
matplotlib.use('Agg')
