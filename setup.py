from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
    "numpy >= 1.15.2",
    "gwpy >= 0.16.0",
    "gpstime >= 0.3.2",
    "Click >= 7.0",
    "PyYAML >= 3.13",
]

# Extra dependencies.
EXTRAS = {
    "wp-alp": [
        "wordpress-api >= 1.2.9",
    ]
}

setup(
    name="pemwatch",
    description="Physical environment monitoring watchdog tools",
    long_description=README,
    version="0.1.0",
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/sean-leavey/pemwatch",
    packages=find_packages(),
    package_data={
        "pemwatch": ["config.yaml.dist"],
    },
    entry_points={
        "console_scripts": [
            "%s = pemwatch.__main__:pemwatch" % "pemwatch",
        ],
    },
    install_requires=REQUIREMENTS,
    extras_require=EXTRAS,
    license="GPLv3",
    zip_safe=False,
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ]
)
